Votechain
=========

Votechain is a PoC voting protocol implemented in Haskell based on blockchain technology.
We used the [Nanocoin](https://github.com/tdietert/nanocoin) implementation as a boilerplate. 

**Project goals**:

* A voter will be able to verify that their vote has been cast correctly and assigned to the right candidate
* A voter shall be able to verify the tally of votes, and ensure that no tampering has been done by the authorities
* A voter shall be anonymous, i.e the vote tally shall be released publicly without revealing the identity of the voters

Running a Node 
---------------

Install the [Stack](https://docs.haskellstack.org/en/stable/README/) build system:

```bash
$ stack setup
$ stack install votechain
$ votechain
```

Running `votechain` will spin up a Node with an RPC server running on `localhost:3000`
and a P2P server communicating with basic UDP Multicast on port `8001`.

You can specify which port to run the RPC server on, and from which directory to load
a node's public/private ECC key pair. If you do not supply a `KEYS_DIR`, the node will
generate a random key pair with which to issue transactions and mine block.

`Usage: votechain [-r|--rpc-port RPC_PORT] [-k|--keys KEYS_DIR] -u <USERNAME> -p <PASSWORD>`

### RPC Interface

Votechain's RPC interface is implemented via an HTTP web server that serves as
both a command and query entry points. Simply type in
`localhost:XXXX/<cmd-or-query>` to interact with the node1:

#### Queries


`/blocks`:
   
    View the blocks on the block chain, including their votes.

`/mempool`:
 
    View the current collected votes that have not yet been included in a
    block on the network.

`/ledger`:
  
    View the current state of the ledger, representative of all the votes
    of all the blocks applied in order to result in a cumulative ledger state.

#### Commands

`/mineBlock`:

    Attempt to mine a block containing the votes currently in the node's mempool.
    This will fail if there are no votes in the mempool. 

`/vote/:toCandidateId`
 
    Issues a `Vote` transaction to the network, transferring the vote
    from this user's account to a candidate designated by `toCandidateId`.
    If you try to vote more than once the vote will be rejected during 
    the block mining process and purged from all nodes' mem-pools.