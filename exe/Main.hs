module Main where

import Prelude (error)
import Protolude hiding (option)

import Data.Maybe (fromMaybe)
import qualified Data.ByteString.Char8 as BSC(pack)

import Votechain (initNode)
import Data.String
import Options.Applicative
import Logger

data Config = Config
  { rpcPort      :: Int
  , keysPath     :: Maybe FilePath
  , logFilepath  :: Maybe FilePath
  , votersFilepath     :: FilePath
  , candidatesFilepath :: FilePath
  , username :: Maybe String
  , password :: Maybe String
  }

defaultConfig :: Config
defaultConfig = Config 3000 Nothing Nothing "credentials" "candidates" Nothing Nothing

main :: IO ()
main = do
    Config rpc mKeys mLogFile voterFile candFile mUsername mPassword <- execParser (info parser mempty)
    logger <- mkLogger mLogFile

    case (mUsername, mPassword) of
      (Just x, Just y) -> initNode rpc mKeys logger voterFile candFile (BSC.pack x) (BSC.pack y)
      _ -> error "Missing username or password"
  where
    portParser :: Parser (Maybe Int)
    portParser = optional $
      option auto $ long "rpc-port"
                 <> short 'r'
                 <> metavar "RPC_PORT"

    keysParser :: Parser (Maybe FilePath)
    keysParser = optional $
      strOption $ long "keys"
               <> short 'k'
               <> metavar "KEYS_DIR"

    logFileParser :: Parser (Maybe FilePath)
    logFileParser = optional $
      strOption $ long "logfile"
              <> short 'f'
              <> metavar "LOG_FILE"

    votersFileParser :: Parser (Maybe FilePath)
    votersFileParser = optional $
      strOption $ long "voters"
               <> short 'v'
               <> metavar "VOTERS_FILE"

    candsFileParser :: Parser (Maybe FilePath)
    candsFileParser = optional $
      strOption $ long "candidates"
               <> short 'c'
               <> metavar "CANDIDATES_FILE"

    usernameParser :: Parser (Maybe String)
    usernameParser = optional $
      strOption $ long "username"
              <> short 'u'
              <> metavar "USERNAME"

    passwordParser :: Parser (Maybe String)
    passwordParser = optional $
      strOption $ long "password"
               <> short 'p'
               <> metavar "password"


    parser = Config
      <$> (fromMaybe (rpcPort defaultConfig) <$> portParser)
      <*> keysParser
      <*> logFileParser
      <*> (fromMaybe (votersFilepath defaultConfig) <$> votersFileParser)
      <*> (fromMaybe (candidatesFilepath defaultConfig) <$> candsFileParser)
      <*> usernameParser
      <*> passwordParser
