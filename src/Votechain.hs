{-# LANGUAGE DeriveAnyClass #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE TupleSections #-}

module Votechain (
  initNode
) where

import Protolude hiding (get, put)
import Web.Scotty
import Logger
import Data.String
import System.Directory (createDirectoryIfMissing, doesFileExist)

import qualified System.Logger as Logger

import qualified Key
import qualified Prelude as Pr(lines, read, readFile)
import qualified Data.ByteString as BS
import qualified Data.ByteString.Char8 as BSC
import qualified Votechain.Block as B
import qualified Votechain.CLI as CLI
import qualified Votechain.Ledger as L
import qualified Votechain.Vote as V
import qualified Votechain.Network.Message as Msg
import qualified Votechain.Network.Node as Node
import qualified Votechain.Network.P2P as P2P
import qualified Votechain.Network.Peer as Peer
import qualified Votechain.Network.RPC as RPC

import qualified Hash

-- | Initializes a node on the network with it's own copy of
-- the blockchain, and invokes a p2p server and an http server.
initNode :: Int -> Maybe FilePath -> Logger.Logger -> FilePath -> FilePath -> ByteString -> ByteString -> IO ()
initNode rpcPort mKeysPath logger voterFilePath candFilePath username password = do
  let peer = Peer.mkPeer rpcPort

  -- Initialize Node Keys
  keys <- case mKeysPath of
    Nothing -> Key.newKeyPair
    Just keysPath -> do
      eNodeKeys <- Key.readKeys keysPath
      case eNodeKeys of
        Left err   -> die $ show err
        Right keys -> pure keys


  -- Initialize List of registered voters
  voters <- do 
    votersList <- readVoters voterFilePath
    case votersList of
      Left err -> die $ show err
      Right voters -> pure voters

  cands <- do 
    candsList <- readCandidates candFilePath
    case candsList of
      Left err -> die $ show err
      Right cands -> pure cands

  let hashId = hashUsernamePassword username password
  case find (== hashId) voters of 
    Nothing -> die $ show "Invalid Credentials"
    Just x  -> return ()


  -- Initialize Genesis Block
  genesisBlock <- do
    eKeys <- Key.readKeys "keys/genesis"
    case eKeys of
      Left err    -> die $ show err
      Right gkeys -> B.genesisBlock gkeys

  -- Initialize NodeState
  nodeState <- Node.initNodeState peer genesisBlock keys voters cands hashId

  -- Fork P2P server
  forkIO $ P2P.p2p nodeState logger
  -- Join network by querying latest block
  joinNetwork $ Node.nodeSender nodeState

  forkIO $ RPC.rpcServer nodeState logger

  -- Run cmd line interface
  CLI.cli nodeState logger

-- | Query the network for the latest block
joinNetwork :: Msg.MsgSender -> IO ()
joinNetwork nodeSender = nodeSender $ Msg.QueryBlockMsg 1

readVoters :: FilePath -> IO (Either String [ByteString])
readVoters fp = do
  fileExists <- doesFileExist fp
  if fileExists
    then do
      content <- BS.readFile fp
      let myLines :: [ByteString] = BSC.lines content
      pure $ Right myLines
    else pure $
      Left "Supplied voters file is invalid"

readCandidates :: FilePath -> IO (Either String [Int])
readCandidates fp = do
  fileExists <- doesFileExist fp
  if fileExists
    then do
      content <- Pr.readFile fp
      let myCandIds :: [Int] = [Pr.read line::Int | line <- (Pr.lines content)]
      pure $ Right myCandIds
    else pure $
      Left "Supplied candidates file is invalid"

hashUsernamePassword :: ByteString -> ByteString -> ByteString
hashUsernamePassword u p =
  Hash.getHash $ Hash.sha256 $
    BS.concat [ u,p ]