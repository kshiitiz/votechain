{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE DeriveAnyClass #-}

module Votechain.Network.Message (
  Msg(..),
  MsgSender,
  MsgReceiver,
) where

import Protolude hiding (msg)

import qualified Data.Serialize as S

import Votechain.Block
import Votechain.Vote
import qualified Votechain.Network.Multicast as M
import qualified Votechain.Network.Peer as Peer

data Msg
  = QueryBlockMsg Int
  | BlockMsg Block
  | VoteMsg Vote
  deriving (Eq, Show, Generic, S.Serialize)

type MsgSender = M.Sender Msg
type MsgReceiver = M.Receiver Msg
