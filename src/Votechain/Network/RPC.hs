
module Votechain.Network.RPC (
  rpcServer
) where

import Protolude hiding (get, intercalate, print, putText)

import Data.Aeson hiding (json)
import Data.Text (intercalate)
import Web.Scotty

import Data.List ((\\))
import qualified Data.Map as Map

import Logger
import Address
import qualified Address

import Votechain.Network.Node
import Votechain.Network.Peer

import qualified Key

import qualified Votechain.Ledger as L
import qualified Votechain.Block as B
import qualified Votechain.MemPool as MP
import qualified Votechain.Vote as V
import qualified Votechain.Network.Message as Msg
-------------------------------------------------------------------------------
-- RPC (HTTP) Server
-------------------------------------------------------------------------------

-- | Starts an RPC server for interaction via HTTP
rpcServer :: NodeState -> Logger -> IO ()
rpcServer nodeState logger = do

  let (Peer hostName p2pPort rpcPort) = nodeConfig nodeState

  scotty rpcPort $ do

    defaultHandler $ logError' logger . toS

    --------------------------------------------------
    -- Queries
    --------------------------------------------------
    get "/blocks" $
      queryNodeState nodeState getBlockChain

    get "/mempool" $
      queryNodeState nodeState getMemPool

    get "/ledger" $
      queryNodeState nodeState getLedger

    --------------------------------------------------
    -- Commands
    --------------------------------------------------

    get "/mineBlock" $ do
      eBlock <- runLogger logger $ mineBlock nodeState
      case eBlock of
        Left err -> text $ show err
        Right block -> json block

    get "/vote/:toCandidate" $ do
      cId <- param "toCandidate"
      json =<< issueTransfer nodeState cId

queryNodeState
  :: ToJSON a
  => NodeState
  -> (NodeState -> IO a)
  -> ActionM ()
queryNodeState nodeState f = json =<< liftIO (f nodeState)
