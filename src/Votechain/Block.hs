{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE DeriveAnyClass #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE NoImplicitPrelude #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE TupleSections #-}

module Votechain.Block (
  Block(..),
  BlockHeader(..),
  Blockchain,
  genesisBlock,

  -- ** Block Hashing
  hashBlock,
  hashBlockHeader,

  -- ** Validation
  InvalidBlock(..),
  validateBlock,
  applyBlock,
  validateAndApplyBlock,

  -- ** Consensus
  proofOfWork,
  validateProofOfWork,
  mineBlock,
  getLatestBlock,


) where

import Protolude

import Control.Monad (fail)

import Data.Aeson
import Data.Aeson.Types (typeMismatch)
import Data.Int (Int64)
import Data.Time.Clock.POSIX (getPOSIXTime)

import qualified Data.ByteArray as BA
import qualified Data.ByteString as BS
import qualified Data.ByteString.Char8 as B8
import qualified Data.Serialize as S
import qualified Data.Text as T
import Data.Text.Encoding as T

import Crypto.Hash.MerkleTree

import Address
import Time (Timestamp, now)
import Votechain.Ledger
import Votechain.Vote (Vote)

import qualified Hash
import qualified Key
import qualified Votechain.Vote as V
import qualified Votechain.Ledger as Ledger

type Index      = Int
type Blockchain = [Block]

data BlockHeader = BlockHeader
  { origin       :: Key.PublicKey -- ^ Address of Block miner
  , previousHash :: ByteString    -- ^ Previous block hash
  , merkleRoot   :: ByteString    -- ^ Merkle Root of transactions
  , nonce        :: Int64         -- ^ Nonce for Proof-of-Work
  } deriving (Eq, Show)

data Block = Block
  { index        :: Index         -- ^ Block height
  , header       :: BlockHeader   -- ^ Block header
  , votes        :: [Vote]        -- ^ List of Votes
  , signature    :: ByteString    -- ^ Block signature
  , timestamp    :: Timestamp     -- ^ POSIX timestamp
  } deriving (Eq, Show, Generic, S.Serialize)

genesisBlock :: Key.KeyPair -> IO Block
genesisBlock (pubKey, privKey) = do
    signature' <- liftIO $
      Key.sign privKey (S.encode genesisBlockHeader)
    return Block
      { index        = 0
      , header       = genesisBlockHeader
      , votes        = []
      , signature    = S.encode signature'
      , timestamp    = 0
      }
  where
    genesisBlockHeader = BlockHeader
      { origin       = pubKey
      , previousHash = "0"
      , merkleRoot   = getMerkleRoot emptyHash
      , nonce        = 0
      }

-- | Get the latest block from the chain
getLatestBlock :: Blockchain -> Maybe Block
getLatestBlock = head

-------------------------------------------------------------------------------
-- Block Hashing
-------------------------------------------------------------------------------

-- | Hash a block header, to be used as the prevHash field in Block
hashBlockHeader :: BlockHeader -> ByteString
hashBlockHeader BlockHeader{..} =
  Hash.getHash $ Hash.sha256 $
    BS.concat [ rawAddress (deriveAddress origin)
              , previousHash
              , merkleRoot
              , B8.pack (show nonce)
              ]

-- | Generate a block hash
hashBlock :: Block -> ByteString
hashBlock = hashBlockHeader . header

-------------------------------------------------------------------------------
-- Validation
-------------------------------------------------------------------------------

data InvalidBlock
  = InvalidBlockSignature Text
  | InvalidBlockIndex Int
  | InvalidBlockHash
  | InvalidBlockMerkleRoot Text
  | InvalidBlockNumTxs
  | InvalidBlockTx V.InvalidVote
  | InvalidPrevBlockHash
  | InvalidFirstBlock
  | InvalidBlockTxs [V.InvalidVote]
  deriving (Show, Eq)

-- | Verify a block's ECDSA signature
verifyBlockSignature
  :: Block
  -> Either InvalidBlock ()
verifyBlockSignature b = do
  let originKey = origin $ header b
  case S.decode (signature b) of
    Left err -> Left $ InvalidBlockSignature (toS err)
    Right sig -> do
      let validSig = Key.verify originKey sig (S.encode $ header b)
      unless validSig $
        Left $ InvalidBlockSignature "Could not verify block signature."

-- | Validate a block before accepting a block as new block in chain
validateBlock
  :: Ledger
  -> Block
  -> Block
  -> Either InvalidBlock ()
validateBlock ledger prevBlock block
  | index block /= index prevBlock + 1 = Left $ InvalidBlockIndex (index block)
  | hashBlock prevBlock /= previousHash (header block) = Left InvalidPrevBlockHash
  | not (validateProofOfWork block) = Left InvalidBlockHash
  | null (votes block) = Left InvalidBlockNumTxs
  | mRoot /= mRoot' = Left $ InvalidBlockMerkleRoot $ toS mRoot'
  | otherwise = do
      -- Verify signature of block
      verifyBlockSignature block
      -- Validate all transactions w/ respect to world state
      first InvalidBlockTx $
        V.validateVotes ledger blockTxs
  where
    blockTxs = votes block
    txHashes = map V.hashVote blockTxs
    mRoot  = merkleRoot $ header block      -- given root
    mRoot' = mtHash $ mkMerkleTree txHashes -- constr root

validateAndApplyBlock
  :: Ledger
  -> Block
  -> Block
  -> Either InvalidBlock (Ledger, [V.InvalidVote])
validateAndApplyBlock ledger prevBlock block = do
  validateBlock ledger prevBlock block
  Right $ applyBlock ledger block

-- | Apply block transactions to world state
applyBlock
  :: Ledger
  -> Block
  -> (Ledger, [V.InvalidVote])
applyBlock ledger = V.applyVotes ledger . votes

-------------------------------------------------------------------------------
-- Consensus
-------------------------------------------------------------------------------

-- | Generates (mines) a new block using the `proofOfWork` function
mineBlock
  :: MonadIO m
  => Block          -- ^ Previous Block in chain
  -> Key.KeyPair    -- ^ Miner's ECDSA key pair
  -> [Vote]  -- ^ List of transactions
  -> m Block
mineBlock prevBlock keys@(pubKey,privKey) txs' = do

    -- Create the block header
    let blockTxs = txs' 
    let blockHeader = mkBlockHeader blockTxs

    -- Get time of block creation
    ts <- liftIO Time.now

    -- Sign the serialized block header
    signature' <- liftIO $
      Key.sign privKey (S.encode blockHeader)
    return Block
      { index        = index'
      , header       = blockHeader
      , votes        = blockTxs
      , signature    = S.encode signature'
      , timestamp    = ts
      }
  where
    index'      = index prevBlock + 1
    prevHash    = hashBlock prevBlock
    origin'     = Key.toPublic privKey

    mkBlockHeader txs = proofOfWork index' $
      let txHashes = map V.hashVote txs
      in BlockHeader { origin       = origin'
                     , previousHash = prevHash
                     , merkleRoot   = mtHash (mkMerkleTree txHashes)
                     , nonce        = 0
                     }

proofOfWork
  :: Int         -- ^ Difficulty measured by block index
  -> BlockHeader -- ^ Header to hash with nonce parameter
  -> BlockHeader
proofOfWork idx blockHeader = blockHeader { nonce = calcNonce 0 }
  where
    difficulty = calcDifficulty idx
    prefix = toS $ replicate difficulty '0'

    calcNonce n
      | prefix' == prefix = n
      | otherwise = calcNonce $ n + 1
      where
        headerHash = hashBlockHeader (blockHeader { nonce = n })
        prefix' = BS.take difficulty headerHash

-- | difficulty(block) = round(ln(index(block)))
calcDifficulty :: Int -> Int
calcDifficulty = round . logBase (2 :: Float) . fromIntegral

validateProofOfWork :: Block -> Bool
validateProofOfWork block =
    BS.isPrefixOf prefix $ hashBlock block
  where
    difficulty = calcDifficulty $ index block
    prefix = toS $ replicate difficulty '0'


now :: IO Integer
now = round <$> getPOSIXTime

-------------------------------------------------------------------------------
-- Serialization
-------------------------------------------------------------------------------

instance S.Serialize BlockHeader where
  put (BlockHeader opk ph mr n) = do
    Key.putPublicKey opk
    S.put ph
    S.put mr
    S.put n
  get = BlockHeader
    <$> Key.getPublicKey
    <*> S.get
    <*> S.get
    <*> S.get

instance ToJSON BlockHeader where
  toJSON (BlockHeader opk ph mr n) =
    object [ "previousHash" .= Hash.encode64 ph
           , "merkleRoot"   .= Hash.encode64 mr
           , "nonce"        .= toJSON n
           ]

instance ToJSON Block where
  toJSON (Block i bh votes s ts) =
    object [ "index"        .= i
           , "header"       .= toJSON bh
           , "votes"        .= toJSON votes
           , "signature"    .= Hash.encode64 s
           , "timestamp"    .= ts
           ]
