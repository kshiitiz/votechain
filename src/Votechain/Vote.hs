{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE DeriveAnyClass #-}

module Votechain.Vote (
  Vote(..),
  VoteHeader(..),

  hashVote,

  -- ** Construction
  voteTransaction,

  -- ** Validation
  InvalidVote(..),
  validateVotes,

  applyVote,
  applyVotes,

  invalidVotes,

) where

import Protolude hiding (throwError)

import Data.Aeson
import Data.Aeson.Types
import qualified Data.ByteString as BS
import qualified Data.ByteString.Char8 as B8
import qualified Data.Serialize as S

import Address (Address, rawAddress, deriveAddress)
import Hash (Hash)
import Time (Timestamp, now)
import Votechain.Ledger (Ledger)

import qualified Hash
import qualified Key
import qualified Votechain.Ledger as Ledger

data VoteHeader = VoteHeader
  { candidateId :: Int
  , senderHash   :: ByteString
  } deriving (Eq, Show, Generic)

data Vote = Vote
  { voteHeader :: VoteHeader
  , timestamp  :: Timestamp
  } deriving (Eq, Show, Generic, S.Serialize)

hashVote :: Vote -> ByteString
hashVote = hashVoteHeader . voteHeader

hashVoteHeader :: VoteHeader -> ByteString
hashVoteHeader = Hash.getHash . Hash.sha256 . S.encode

-------------------------------------------------------------------------------
-- Vote Construction
-------------------------------------------------------------------------------

vote
  :: VoteHeader
  -> IO Vote
vote voteHdr = do
  ts <- Time.now
  pure $ Vote voteHdr ts

voteTransaction
  :: Int     -- ^ Address of recipient
  -> ByteString         -- ^ VoteToken
  -> IO Vote
voteTransaction candidateId senderHash =
  vote $ VoteHeader candidateId senderHash

-------------------------------------------------------------------------------
-- Validation & Application of Transactions
-------------------------------------------------------------------------------

data InvalidVote = InvalidVote Vote Ledger.VoteError
  deriving (Show, Eq, Generic, ToJSON)

-- | Validate all votes with respect to world state
validateVotes :: Ledger -> [Vote] -> Either InvalidVote ()
validateVotes ledger votes =
  case snd (applyVotes ledger votes) of
    []     -> Right ()
    (x:xs) -> Left x

type ApplyM = State [InvalidVote]

throwError :: InvalidVote -> ApplyM ()
throwError ivote = modify (ivote:)

runApplyM :: ApplyM a -> (a,[InvalidVote])
runApplyM = flip runState []

-- | Applies a list of votes to the ledger
applyVotes
  :: Ledger
  -> [Vote]
  -> (Ledger,[InvalidVote])
applyVotes ledger =
  runApplyM . foldM applyVote ledger

-- | Applies a vote to the ledger state
applyVote
  :: Ledger
  -> Vote
  -> ApplyM Ledger
applyVote ledger vt@(Vote hdr ts) = do
  case Ledger.transfer sHash cId ledger of
    Left err -> do
      throwError $ InvalidVote vt err
      pure ledger
    Right ledger' -> pure ledger'
  where
    sHash = senderHash hdr
    cId = candidateId hdr

invalidVotes :: [InvalidVote] -> [Vote]
invalidVotes ivotes = flip map ivotes $ \(InvalidVote vote _) -> vote

-------------------------------------------------------------------------------
-- Serialization
-------------------------------------------------------------------------------

instance S.Serialize VoteHeader where
  put (VoteHeader candidateId senderHash) = do
    S.put candidateId
    S.put senderHash
  get = VoteHeader
    <$> S.get
    <*> S.get

instance ToJSON VoteHeader where
  toJSON (VoteHeader candidateId senderHash) =
    object [ "candidateId" .= toJSON candidateId
           -- , "senderHash"    .= toJSON senderHash
           ]

instance ToJSON Vote where
  toJSON (Vote hdr ts) =
    object [ "vote"    .= toJSON hdr
           , "timestamp" .= ts
           ]
