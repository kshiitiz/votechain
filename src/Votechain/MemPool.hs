{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}

module Votechain.MemPool (
  MemPool(..),
  addVote,
  removeVotes,
) where

import Protolude

import Data.Aeson
import Data.List ((\\))

import Votechain.Vote (Vote)

newtype MemPool = MemPool
  { unMemPool :: [Vote]
  } deriving (Show, Eq, Generic, Monoid, ToJSON)

addVote
  :: Vote
  -> MemPool
  -> MemPool
addVote vt (MemPool pool) =
  MemPool (pool ++ [vt])

removeVotes
  :: [Vote]
  -> MemPool
  -> MemPool
removeVotes vts (MemPool pool) =
  MemPool $ pool \\ vts
