{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE DeriveAnyClass #-}

module Votechain.Ledger (
  Ledger(..),

  transfer,
  VoteError,

) where

import Protolude

import Data.Aeson
import Data.Map (Map)
import Data.List (find)
import Data.ByteString (ByteString)
import qualified Data.Map as Map
import qualified Data.Serialize as S

import Address
import qualified Key

data VoteError
  = AlreadyVoted 
  deriving (Eq, Show, Generic)

instance ToJSON VoteError
-- TODO: Change address to Hash
-- | Datatype storing the holdings of addresses
data Ledger = Ledger
  { unLedger :: [ByteString]
  , unCandidateLedger :: Map Int Int 
  } deriving (Eq, Show, Generic)


instance ToJSON Ledger where
  toJSON l = let u1 = unLedger l
                 u2 = unCandidateLedger l
             in toJSON (u2)    
             -- in toJSON (u1, u2)    

emptyLedger :: Ledger
emptyLedger = Ledger { unLedger = []
                     , unCandidateLedger = mempty
                     }

instance Monoid Ledger where
  mempty = emptyLedger 
  mappend l1 l2 = Ledger { unLedger = (unLedger l1) ++ (unLedger l2)
                     , unCandidateLedger = mappend (unCandidateLedger l1) (unCandidateLedger l2)
                     }

-- Check if a voter has already voted. This operation returns
-- True if the hashed address has never voted on the network.
canVote :: ByteString -> Ledger -> Bool
canVote senderHash ledger = 
  case find (== senderHash) $ unLedger ledger of
    Nothing -> True
    Just a -> False

-- | Add a vote to a candidate's existing count
addVote :: Int -> Ledger -> Ledger
addVote candidateId ledger =
  Ledger voterLed candLed
  where candLed = Map.adjust (+1) candidateId $ unCandidateLedger ledger
        voterLed = unLedger ledger

-- | Add the sender's hash to the voter ledger
spendVote :: ByteString -> Ledger -> Ledger
spendVote senderHash ledger = Ledger voterLed candLed
  where candLed = unCandidateLedger ledger
        voterLed = senderHash : (unLedger ledger)

-- | Send vote from voter to Candidate
transfer
  :: ByteString
  -> Int
  -> Ledger
  -> Either VoteError Ledger
transfer sHash cId ledger = do
  case canVote sHash ledger of
    False  -> Left $ AlreadyVoted
    True -> Right $ spendVote sHash $ addVote cId ledger
