{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE ScopedTypeVariables #-}

module Votechain.CLI (
  cli
) where

import Protolude
import Prelude (words)

import qualified Data.Map as Map

import Options.Applicative

import System.Console.Haskeline hiding (defaultPrefs, catch)

import Logger

import Votechain.Network.Node (NodeState)
import qualified Votechain.Ledger as L
import qualified Votechain.MemPool as MP
import qualified Votechain.Network.Node as Node

data Query
  = QueryBlocks
  | QueryMemPool
  | QueryLedger
  deriving (Show)

data Cmd
  = CmdMineBlock
  | CmdVote Int
  deriving (Show)

data CLI
  = Query Query
  | Command Cmd
  deriving (Show)

cli :: NodeState -> Logger -> IO ()
cli nodeState logger = runInputT defaultSettings cliLoop
  where
    parserPrefs = defaultPrefs
      { prefShowHelpOnEmpty = True }

    cliLoop = do
      minput <- getInputLine "votechain> "
      case minput of
        Nothing -> cliLoop
        Just input -> do
          let cliInputArgs = words input
          mCmdOrQuery <- liftIO $ handleParseResult_ $
            execParserPure parserPrefs (info cliParser fullDesc) cliInputArgs
          case mCmdOrQuery of
            Nothing -> cliLoop
            Just cmdOrQuery -> do
              liftIO $ runLogger logger $
                handleCLI nodeState cmdOrQuery
              cliLoop

cliParser :: Parser CLI
cliParser = subparser $ mconcat
    [ command "query" $ info (Query <$> queryParser) $
        progDesc "Query the node's state"
    , command "command" $ info (Command <$> cmdParser) $
        progDesc "Issue a command to the node"
    ]
  where
    queryParser = subparser $ mconcat
      [ command "blocks"  $ info (pure QueryBlocks) $
          progDesc "Query the node's blocks"
      , command "mempool" $ info (pure QueryMemPool) $
          progDesc "Query the node's transaction pool"
      , command "ledger"  $ info (pure QueryLedger) $
          progDesc "Query the node's ledger"
      ]

    cmdParser = subparser $ mconcat
        [ command "mineblock" $ info (pure CmdMineBlock) $
            progDesc "Mine a block"
        , command "vote"  $ info transfer $
            progDesc "Vote for CandidateID"
        ]
      where
        transfer = CmdVote
          <$> argument auto (metavar "CANDIDATE_ID")

-- | Handle a cmd line args parser result, discarding the ExitCode
-- XXX Is this ok to do? Just make sure no one shells into the repl.
handleParseResult_ :: ParserResult CLI -> IO (Maybe CLI)
handleParseResult_ pr =
  fmap Just (handleParseResult pr) `catch` \(e :: ExitCode) ->
    pure Nothing

handleCLI
  :: (MonadIO m, MonadLogger m)
  => NodeState
  -> CLI
  -> m ()
handleCLI nodeState cli =

  case cli of

    Query query ->

      case query of

        QueryBlocks  -> do
          blocks <- Node.getBlockChain nodeState
          mapM_ print blocks

        QueryMemPool -> do
          mempool <-  Node.getMemPool nodeState
          let mempool' = MP.unMemPool mempool
          if null mempool'
            then putText "Mempool is empty"
            else myZipWithM_ [1..] mempool' $ \n tx ->
              putText $ show n <> ") " <> show tx

        QueryLedger  -> do
          ledger <- Node.getLedger nodeState
          let voterLedger' = L.unLedger ledger
              candidateLedger' = Map.toList $ L.unCandidateLedger ledger
          putText "Votes registered:"
          if null voterLedger'
            then putText "No votes found :("
            else forM_ voterLedger' $ \senderHash ->
              putText $ show senderHash
          putText "Candidates' registered:"
          if null candidateLedger'
            then putText "No Candidates registered"
            else forM_ candidateLedger' $ \(candId,voteCount) ->
              putText $ show candId <> " : " <> show voteCount

    Command cmd ->

      case cmd of

        CmdMineBlock        -> do
          eBlock <- Node.mineBlock nodeState
          case eBlock of
            Left err    -> logError ("Failed to mine block: " <> show err :: Text)
            Right block -> putText "Successfully mined block: " >> print block

        CmdVote cId -> do
          tx <- Node.issueTransfer nodeState cId
          putText "Issued Transfer: " >> print tx

myZipWithM_ xs ys f = zipWithM_ f xs ys
